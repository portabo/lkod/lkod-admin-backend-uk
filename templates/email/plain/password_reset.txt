Dobrý den,

vypadá to, že jste zapomněl heslo k účtu v Golemio LKOD. Pro nastavení nového hesla otevřete v prohlížeči následující url adresu {{urlWithCode}}.

Pokud heslo měnit nechcete, tak tento email ignorujte.

S pozdravem,
Tým Golemio
