import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "express";
import "mocha";
import { redisConnector } from "../../../src/core/database";
import { tokenManager } from "../../../src/core/helpers";
import { AuthenticationMiddleware } from "../../../src/core/middlewares";
import { log } from "../../../src/core/helpers";

const express = require("express");
const request = require("supertest");

describe("AuthenticationMiddleware", () => {
    // Create clean express instance
    const app = express();
    app.use(
        express.json({
            limit: "2MB", // Reject payload bigger than limit
        })
    );

    before(async () => {
        // Mount the tested router to the express instance
        app.use("/secret", new AuthenticationMiddleware().authenticate, (req: Request, res: Response, next: NextFunction) => {
            res.json({ secret: true });
        });
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
        await redisConnector.connect();
    });

    it("should respond with 200 to GET /secret", () => {
        return tokenManager
            .generateToken(
                {
                    id: 1,
                    email: "test@golemio.cz",
                    name: "test user",
                    role: "user",
                },
                300
            )
            .then((token) => request(app).get("/secret").set("Authorization", `Bearer ${token}`).expect(200));
    });

    it("should respond with 401 if authorization header is not provided", (done) => {
        request(app)
            .get("/secret")
            .expect(401)
            .end((err: any, res: Request) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 401 if authorization header is not valid", (done) => {
        request(app)
            .get("/secret")
            .set("Authorization", "Bearer testtest")
            .expect(401)
            .end((err: any, res: Request) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 401 if token was revoked", (done) => {
        tokenManager
            .generateToken({ test: "test" }, 300)
            .then((token) => {
                return Promise.all([token, tokenManager.verifyAndDecodeToken(token) as any]);
            })
            .then(([token, decoded]) => {
                return Promise.all([token, tokenManager.setTokenToBlacklist(decoded.jti, decoded.exp)]);
            })
            .then(([token, set]) => {
                request(app)
                    .get("/secret")
                    .set("Authorization", "Bearer " + token)
                    .expect(401)
                    .end((err: any, res: Request) => {
                        if (err) {
                            return done(err);
                        }
                        done();
                    });
            });
    });
});
