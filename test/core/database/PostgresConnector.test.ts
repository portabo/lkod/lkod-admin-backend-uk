import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { expect } from "chai";
import "mocha";
import { postgresConnector } from "../../../src/core/database";

describe("PostgresConnector", () => {
    it("should has connect method", async () => {
        expect(postgresConnector.connect).not.to.be.undefined;
    });

    it("should has getConnection method", async () => {
        expect(postgresConnector.getConnection).not.to.be.undefined;
    });

    it("should has connect method", async () => {
        expect(postgresConnector.connect).not.to.be.undefined;
    });

    it("should has getConnection method", async () => {
        expect(postgresConnector.getConnection).not.to.be.undefined;
    });

    it("should throws Error if not connect method was not called", () => {
        expect(postgresConnector.getConnection).to.throw(CustomError);
    });

    it("should connects to PostgreSQL and returns connection", async () => {
        const ch = await postgresConnector.connect();
        expect(ch).to.be.an.instanceof(Object);
    });

    it("should returns connection", async () => {
        await postgresConnector.connect();
        expect(postgresConnector.getConnection()).to.be.an.instanceof(Object);
    });
});
