import { expect } from "chai";
import "mocha";
import { dataValidator } from "../../../src/core/helpers";

describe("dataValidator", () => {
    it("should has validate method", async () => {
        expect(dataValidator.validate).not.to.be.undefined;
    });

    it("should validate data and return result (valid)", async () => {
        const result = dataValidator.validate(require("./data/valid_dataset.json"));
        expect(result).to.deep.equal({
            status: "valid",
            messages: null,
        });
    });

    it("should validate data and return result (invalid)", async () => {
        const result = dataValidator.validate(require("./data/invalid_dataset.json"));
        expect(result).to.deep.equal({
            status: "invalid",
            messages: [
                "data musí obsahovat požadovanou položku poskytovatel",
                "data musí obsahovat požadovanou položku téma",
                "data musí obsahovat požadovanou položku periodicita_aktualizace",
                "data musí obsahovat požadovanou položku klíčové_slovo",
            ],
        });
    });

    it("should validate data and return result (hasWarning)", async () => {
        const result = dataValidator.validate(require("./data/hasWarnings_dataset.json"));
        expect(result).to.deep.equal({
            status: "hasWarnings",
            messages: [
                'data/periodicita_aktualizace musí vyhovět regulárnímu výrazu "^http\\://publications\\.europa\\.eu/resource/authority/frequency/.*$"',
            ],
        });
    });
});
