import { expect } from "chai";
import "mocha";
import { config } from "../../../src/core/config";
import { postgresConnector } from "../../../src/core/database";
import { log, logActivity } from "../../../src/core/helpers";
import { IActivityLogAttributes, IActivityLogInput } from "../../../src/models/ActivityLogModel";
import { init } from "../../resources/00_init.test";

describe("Logger", () => {
    it("should has silly method", () => {
        expect(log.silly).not.to.be.undefined;
    });

    it("should has debug method", () => {
        expect(log.debug).not.to.be.undefined;
    });

    it("should has info method", () => {
        expect(log.info).not.to.be.undefined;
    });

    it("should has warn method", () => {
        expect(log.warn).not.to.be.undefined;
    });

    it("should has error method", () => {
        expect(log.error).not.to.be.undefined;
    });

    describe("Activity Log", () => {
        before(async () => {
            // mainly database initialization
            await init();

            config.activity_log_enabled = true;
        });

        it("should properly log activity into DB", async () => {
            const testObject: IActivityLogInput = {
                scope: "Test",
                action: "save",
                userId: 1,
                meta: {
                    test: "data",
                },
            };

            await logActivity(testObject);

            const result = await postgresConnector.getConnection().query(`SELECT * FROM activity_log`, { raw: true });
            const { id, didAt, ...rest } = result[0][0] as IActivityLogAttributes;
            expect(rest).to.be.deep.equal(testObject);
            expect(id).to.be.a("number").equal(1);
            expect(didAt).to.be.a("Date");
        });
    });
});
