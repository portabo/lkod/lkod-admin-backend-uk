import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as sinon from "sinon";
import * as supertest from "supertest";
import { log } from "../../src/core/helpers";
import { ForgotPasswordTokenModel, IForgotPasswordTokenAttributes } from "../../src/models/ForgotPasswordTokenModel";
import { AuthRouter } from "../../src/resources/auth";
import { init } from "./00_init.test";

describe("Forgot password", () => {
    let sandbox: sinon.SinonSandbox;
    // Create clean express instance
    const app = express();
    let authRouter: AuthRouter;
    let token: string | undefined;

    before(async () => {
        // mainly database initialization
        await init();

        authRouter = new AuthRouter();

        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        // Mount the tested router to the express instance
        app.use("/auth", authRouter.router);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // hadle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should create request to forgot password at POST /auth/forgot-password", (done) => {
        const url = "/auth/forgot-password";
        supertest(app)
            .post(url)
            .send({
                email: "new@golemio.cz",
            })
            .expect(202)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                ForgotPasswordTokenModel.findOne({ where: { userId: 4 } })
                    .then((tk) => {
                        token = tk?.id;
                        done();
                    })
                    .catch((err) => done(err));
            });
    });

    it("should throws error if bad data was send at POST /auth/forgot-password", (done) => {
        const url = "/auth/forgot-password";
        supertest(app)
            .post(url)
            .send({
                userId: "test@golemio.cz",
            })
            .expect(400, done);
    });

    it("should throws error if user was not found at POST /auth/forgot-password", (done) => {
        const url = "/auth/forgot-password";
        supertest(app)
            .post(url)
            .send({
                email: "test@operatorict.cz",
            })
            .expect(401, done);
    });

    it("should reset password at POST /auth/reset-password", (done) => {
        const url = "/auth/reset-password";
        supertest(app)
            .post(url)
            .send({
                token,
                password: "resetpass",
            })
            .expect(204)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                supertest(app)
                    .post("/auth/login")
                    .send({
                        email: "new@golemio.cz",
                        password: "resetpass",
                    })
                    .expect(200)
                    .end((err, res) => {
                        if (err) {
                            done(err);
                        }
                        chai.expect(res.body.hasDefaultPassword).to.be.eq(false);
                        chai.expect(err).to.be.null;
                        done();
                    });
            });
    });

    it("should throws error if bad data was send at POST /auth/reset-password", (done) => {
        const url = "/auth/reset-password";
        supertest(app)
            .post(url)
            .send({
                token: "abcdef",
                password: "resetpass",
            })
            .expect(400, done);
    });

    it("should throws error if token was not found at POST /auth/reset-password", (done) => {
        const url = "/auth/reset-password";
        supertest(app)
            .post(url)
            .send({
                token,
                password: "resetpass",
            })
            .expect(401, done);
    });

    it("should throws error if token was exprired at POST /auth/reset-password", (done) => {
        ForgotPasswordTokenModel.create({
            id: "4fc8a020-4f2c-4dd8-9b3c-9967e2321feb",
            userId: 1,
            validTo: new Date("2022-07-15T10:00:00"),
        } as IForgotPasswordTokenAttributes)
            .then(() => {
                const url = "/auth/reset-password";
                supertest(app)
                    .post(url)
                    .send({
                        token: "4fc8a020-4f2c-4dd8-9b3c-9967e2321feb",
                        password: "resetpass",
                    })
                    .expect(401, done);
            })
            .catch((err) => done(err));
    });
});
