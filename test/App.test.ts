import { expect } from "chai";
import * as express from "express";
import "mocha";
import * as sinon from "sinon";
import * as request from "supertest";
import App from "../src/App";
import { config } from "../src/core/config";

describe("App", () => {
    let expressApp: express.Application;
    let app: App;
    let sandbox: any;
    let exitStub: any;

    before(async () => {
        app = new App();
        await app.start();
        expressApp = app.express;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        exitStub = sandbox.stub(process, "exit");
    });

    afterEach(() => {
        sandbox.restore();
    });

    after(async () => {
        await app.stop();
    });

    it("should start", async () => {
        expect(expressApp).not.to.be.undefined;
    });

    it("should have all config variables set", () => {
        expect(config).not.to.be.undefined;
        expect(config.database.host).not.to.be.undefined;
    });

    it("should have health check on /", (done) => {
        request(expressApp)
            .get(`${config.pathPrefix}/`)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should have health check on /health", (done) => {
        request(expressApp)
            .get(`${config.pathPrefix}/health`)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should throw not found on /non-exists", (done) => {
        request(expressApp).get(`${config.pathPrefix}/non-exists`).expect(404, done);
    });
});
