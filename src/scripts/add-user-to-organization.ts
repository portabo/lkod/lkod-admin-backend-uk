import winston = require("winston");
const argv = require("minimist")(process.argv.slice(2));

const { printf } = winston.format;
const myFormat = printf(({ message }) => {
    return `${message}`;
});
const logger = winston.createLogger({
    format: myFormat,
    level: "info",
    transports: [new winston.transports.Console()],
});

(async () => {
    if (argv.help || argv.h) {
        logger.info(`\nExample usage: \n\n > bin/lkod.js add-user-to-organization --userId="1" --organizationId="1" \n\n`);
        return;
    }

    if (!argv.userId) {
        logger.error(`User id is required.`);
        return;
    }

    if (!argv.organizationId) {
        logger.error(`Organization id is required.`);
        return;
    }

    logger.info(
        `\n\n` +
            `-- -------------------------------------\n` +
            `INSERT INTO "user_organization" \n` +
            `("userId", "organizationId") \n` +
            `VALUES \n` +
            `(` +
            `'${argv.userId}',` +
            `'${argv.organizationId}'` +
            `); \n` +
            `-- -------------------------------------\n`
    );
})();
