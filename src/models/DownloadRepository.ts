import { QueryTypes } from "sequelize";
import { config } from "../core/config";
import { postgresConnector, SequelizeConnector } from "../core/database";
import { log } from "../core/helpers";
import { ICsvDatasetLine } from "./interfaces/ICsvDatasetLine";

export class DownloadRepository {
    private connector: SequelizeConnector;
    private catalogFrontendHostname: string;
    private backendHostname: string;

    constructor() {
        this.connector = postgresConnector;
        this.catalogFrontendHostname = this.tryGetHostname(config.catalog_frontend_url);
        this.backendHostname = this.tryGetHostname(config.backend_url);
    }

    public getAllDatasets = async (userId: number): Promise<ICsvDatasetLine[]> => {
        const result = await this.connector.getConnection().query<ICsvDatasetLine>(
            `select
                o."name" as "název organizace",
                case when status in ('unpublished', 'saved')
                    then ''
                    else 'https://${this.catalogFrontendHostname}/datasets/https%3A%2F%2F${this.backendHostname}%2Flod%2Fcatalog%2F' || d.id 
                end as "URL datové sady",
                (d.data->>'název')::jsonb->>'cs' as "název datové sady",
                status as "stav datové sady",
                ((jsonb_array_elements((d.data->>'distribuce')::jsonb))->>'název')::jsonb->>'cs' as "název distribuce",
                replace((jsonb_array_elements((d.data->>'distribuce')::jsonb))->>'formát', 'http://publications.europa.eu/resource/authority/file-type/', '') as "formát distribuce",
                replace((d.data->>'kontaktní_bod')::jsonb->>'e-mail', 'mailto:', '') as "kontaktní bod - e-mail",
                ((d.data->>'kontaktní_bod')::jsonb->>'jméno')::jsonb->>'cs' as "kontaktní bod - jméno"
            from
                public.dataset d
            join public.organization o on
                o.id = d."organizationId"
            inner join public.user_organization uo on 
                uo."organizationId" = d."organizationId" and uo."userId" = ${userId}
            where status not in ('deleted', 'created')`,
            {
                type: QueryTypes.SELECT,
            }
        );

        return result;
    };

    private tryGetHostname = (url: string): string => {
        try {
            return new URL(url).hostname;
        } catch (err) {
            log.warn("Unable to parse URL: " + url);
            return url;
        }
    };
}
