import { DataTypes, Model, ModelAttributes } from "sequelize";

export interface ISessionAttributes {
    id: string;
    userId: number;
    datasetId: string;
    createdAt: Date;
    expiresIn: number;
}

export const SessionSchema: ModelAttributes = {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
    },
    userId: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    datasetId: {
        type: DataTypes.UUID,
        allowNull: false,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
    },
    expiresIn: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
};

/**
 * Session Model
 * table: session
 */
export class SessionModel extends Model<ISessionAttributes> implements ISessionAttributes {
    public id: string;
    public userId: number;
    public datasetId: string;
    public createdAt: Date;
    public expiresIn: number;
}
