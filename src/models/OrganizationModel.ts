import { Association, DataTypes, Model, ModelAttributes } from "sequelize";
import { UserModel } from "./UserModel";

export interface IOrganizationAttributes {
    id: number;
    name: string;
    identificationNumber: string;
    slug: string;
    logo: string | null;
    description: string | null;
    arcGisFeed: string | null;

    hasArcGisFeed?: boolean;

    // associations
    users?: UserModel[] | null;
}

export const OrganizationSchema: ModelAttributes = {
    id: {
        autoIncrement: true,
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    identificationNumber: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    slug: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    logo: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true,
    },
    arcGisFeed: {
        type: DataTypes.STRING,
        allowNull: true,
    },
};

/**
 * Organization Model
 * table: organization
 */
export class OrganizationModel extends Model<IOrganizationAttributes> implements IOrganizationAttributes {
    public id: number;
    public name: string;
    public identificationNumber: string;
    public slug: string;
    public logo: string | null;
    public description: string | null;
    public arcGisFeed: string | null;

    public readonly hasArcGisFeed?: boolean;

    public readonly users?: UserModel[] | null;

    public static associations: {
        users: Association<UserModel, OrganizationModel>;
    };
}
