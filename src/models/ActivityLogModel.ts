import { DataTypes, Model, ModelAttributes } from "sequelize";

export interface IActivityLogInput {
    scope: string;
    action: string;
    userId: number;
    meta?: any | null;
}

export interface IActivityLogAttributes {
    id: number;
    scope: string;
    action: string;
    userId: number;
    didAt: Date;
    meta?: any | null;
}

export const ActivityLogSchema: ModelAttributes = {
    id: {
        autoIncrement: true,
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
    },
    scope: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    action: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    userId: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    didAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
    },
    meta: {
        type: DataTypes.JSON,
        allowNull: true,
    },
};

/**
 * ActivityLog Model
 * table: activity_log
 */
export class ActivityLogModel extends Model<IActivityLogAttributes> implements IActivityLogAttributes {
    public id: number;
    public scope: string;
    public action: string;
    public userId: number;
    public didAt: Date;
    public meta?: any | null;
}
