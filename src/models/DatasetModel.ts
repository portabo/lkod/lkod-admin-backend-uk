import { Association, DataTypes, Model, ModelAttributes } from "sequelize";
import { IValidationResult } from "../core/helpers";
import { OrganizationModel } from "./OrganizationModel";
import { UserModel } from "./UserModel";

export enum DatasetStatus {
    created = "created",
    saved = "saved",
    published = "published",
    unpublished = "unpublished",
    deleted = "deleted",
}

export enum DatasetStatusToSet {
    published = "published",
    unpublished = "unpublished",
}

export interface IDatasetData {
    // required properties
    "@context": string;
    iri: string;
    poskytovatel: string;
    název: {
        cs?: string | null;
        en?: string | null;
    };
    klíčové_slovo: {
        cs?: string[] | null;
        en?: string[] | null;
    };

    // some additional properties
    [key: string]: any;
}

export interface IDatasetAttributes {
    id: string;
    userId: number;
    organizationId: number;
    status: DatasetStatus;
    createdAt: Date;
    updatedAt?: Date | null;
    data?: IDatasetData | null;
    validationResult?: IValidationResult | null;

    // inner data json properties
    name?: string | null;
    keywords?: string[] | null;

    // virtual properties
    isReadOnly: boolean;

    // associations
    user: UserModel;
    organization: OrganizationModel;
}

export const DatasetSchema: ModelAttributes = {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
    },
    organizationId: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    userId: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    status: {
        type: DataTypes.ENUM,
        values: Object.values(DatasetStatus),
        allowNull: false,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
    },
    data: {
        type: DataTypes.JSONB,
        allowNull: true,
    },
    validationResult: {
        type: DataTypes.JSON,
        allowNull: true,
    },
    isReadOnly: {
        type: DataTypes.VIRTUAL,
        get(this: DatasetModel): boolean {
            return typeof this.organization?.arcGisFeed === "string";
        },
    },
};

/**
 * Dataset Model
 * table: dataset
 */
export class DatasetModel extends Model<IDatasetAttributes> implements IDatasetAttributes {
    public id: string;
    public userId: number;
    public organizationId: number;
    public status: DatasetStatus;
    public createdAt: Date;
    public updatedAt?: Date | null;
    public data?: IDatasetData | null;
    public validationResult?: IValidationResult | null;

    public readonly name?: string | null;
    public readonly keywords?: string[] | null;

    // virtual properties
    public readonly isReadOnly: boolean;

    public readonly user: UserModel;
    public readonly organization: OrganizationModel;

    public static associations: {
        user: Association<DatasetModel, UserModel>;
        organization: Association<DatasetModel, OrganizationModel>;
    };
}
