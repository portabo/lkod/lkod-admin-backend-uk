import { Association, DataTypes, Model, ModelAttributes } from "sequelize";
import { UserModel } from "./UserModel";

export interface IForgotPasswordTokenAttributes {
    id: string;
    userId: number;
    createdAt: Date;
    validTo: Date;

    // associations
    user: UserModel;
}

export const ForgotPasswordTokenSchema: ModelAttributes = {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
    },
    userId: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
    },
    validTo: {
        type: DataTypes.DATE,
        allowNull: true,
    },
};

/**
 * ForgotPasswordToken Model
 * table: forgot_password_token
 */
export class ForgotPasswordTokenModel extends Model<IForgotPasswordTokenAttributes> implements IForgotPasswordTokenAttributes {
    public id: string;
    public userId: number;
    public createdAt: Date;
    public validTo: Date;

    public readonly user: UserModel;

    public static associations: {
        user: Association<ForgotPasswordTokenModel, UserModel>;
    };
}
