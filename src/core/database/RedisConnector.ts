import { CustomError, ErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import * as Redis from "ioredis";
import { config } from "../config";
import { log } from "../helpers";

/**
 * Redis Connector class to handle connections to Redis.
 * Uses ioredis package.
 */
class RedisConnector {
    private connection: Redis.Redis | undefined;

    /**
     * Creates connection to Redis
     *
     * @returns {Promise<Redis.Redis>}
     */
    public connect = async (): Promise<Redis.Redis> => {
        try {
            if (this.connection) {
                return this.connection;
            }

            if (!config.redis_connection) {
                throw new CustomError("The ENV variable REDIS_CONN cannot be undefined.", true, this.constructor.name, 500);
            }

            this.connection = new Redis(config.redis_connection);

            if (!this.connection) {
                throw new Error("Connection is undefined.");
            }

            this.connection.on("error", (err) => {
                ErrorHandler.handle(
                    new CustomError("Error while connecting to Redis.", false, this.constructor.name, 500, err),
                    log
                );
            });
            this.connection.on("connect", () => {
                log.info("Connected to Redis!");
            });

            return this.connection;
        } catch (err) {
            throw new CustomError("Error while connecting to Redis.", false, this.constructor.name, 500, err);
        }
    };

    /**
     * Returns connection to Redis
     *
     * @returns {Redis.Redis}
     */
    public getConnection = (): Redis.Redis => {
        if (!this.connection) {
            throw new CustomError(
                "Redis connection does not exists. First call connect() method.",
                false,
                this.constructor.name,
                500
            );
        }
        return this.connection;
    };
}

const redisConnector = new RedisConnector();

export { redisConnector };
