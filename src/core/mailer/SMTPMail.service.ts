import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { createTransport, Transporter } from "nodemailer";
import { IMailer, IMailerSendOptions } from "./Mailer";
import { log } from "../helpers";

export interface ISMTPMailConfig {
    pass: string;
    user: string;
    host: string;
    port: number;
    secure: boolean;
    rejectUnauthorized: boolean;
}

export class SMTPMailService implements IMailer {
    private transporter: Transporter;

    constructor(config: ISMTPMailConfig) {
        this.transporter = createTransport({
            auth: {
                user: config.user,
                pass: config.pass,
            },
            host: config.host,
            port: config.port,
            secure: config.secure, // default false
            ...(!config.rejectUnauthorized ? { tls: { rejectUnauthorized: false } } : {}), // default true
        });
    }

    public async send(mailOpts: IMailerSendOptions): Promise<void> {
        try {
            const info = await this.transporter.sendMail(mailOpts);
            log.verbose("[Mailer] Message sent: " + info.messageId);
        } catch (err) {
            log.debug(err);
            (err as Error).message += `, Mail to send: ${JSON.stringify(mailOpts)}`;
            throw new CustomError(`Error while sending an email.`, true, this.constructor.name, 500, err);
        }
    }
}
