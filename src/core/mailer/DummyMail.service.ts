import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { createTransport, Transporter } from "nodemailer";
import { IMailer, IMailerSendOptions } from "./Mailer";
import { log } from "../helpers";

export class DummyMailService implements IMailer {
    private transporter: Transporter;

    constructor() {
        // use simple text log
        this.transporter = createTransport({
            streamTransport: true,
            newline: "unix",
            buffer: true,
        });
    }

    public async send(mailOpts: IMailerSendOptions): Promise<void> {
        try {
            const info = await this.transporter.sendMail(mailOpts);
            log.verbose("[Mailer] Message sent: " + info.messageId);
            log.debug(info.envelope);
            log.debug(info.message.toString());
        } catch (err) {
            (err as Error).message += `, Mail to send: ${JSON.stringify(mailOpts)}`;
            throw new CustomError(`Error while sending an email.`, true, this.constructor.name, 500, err);
        }
    }
}
