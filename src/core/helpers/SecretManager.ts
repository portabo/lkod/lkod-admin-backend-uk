import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { compare, hash } from "bcrypt";

/**
 * Secret Manager class to manage bcrypt hashes.
 */
class SecretManager {
    /** cost factor */
    private saltRounds: number;

    constructor() {
        this.saltRounds = 10;
    }

    /**
     * Creates bcrypt hash from plain string
     *
     * @param {string} plainSecret
     * @returns {Promise<string>} hashed secret
     */
    public async hashSecret(plainSecret: string): Promise<string> {
        try {
            const hashed = await hash(plainSecret, this.saltRounds);
            return hashed;
        } catch (err) {
            throw new CustomError("Error while hashing secret.", true, this.constructor.name, 500, err);
        }
    }

    /**
     * Compares plain string and bcrypt hash
     *
     * @param {string} plainSecret
     * @param {string} hashed
     * @returns {Promise<boolean>}
     */
    public async checkSecret(plainSecret: string, hashed: string): Promise<boolean> {
        try {
            return await compare(plainSecret, hashed);
        } catch (err) {
            throw new CustomError("Error while checking secret.", true, this.constructor.name, 500, err);
        }
    }
}

const secretManager = new SecretManager();

export { secretManager };
