import { CustomError, ErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { CronJob } from "cron";
import { isValidCron } from "cron-validator";
import { log } from "../helpers";

export interface ICronTask {
    name: string;
    cronTime: string;
    process(): Promise<void>;
}

/**
 * Cron Tasks Manager class to handle all automatic tasks
 */
class CronTasksManger {
    private activeCrons: { [name: string]: CronJob } = {};

    /**
     * Registers new cron task
     *
     * @param {ICronTask} task
     */
    public registerTask(task: ICronTask): void {
        if (!isValidCron(task.cronTime, { seconds: true })) {
            throw new CustomError(`Invalid crontime ${task.name} ${task.cronTime}`, true, this.constructor.name);
        }

        this.activeCrons[task.name] = new CronJob(
            task.cronTime,
            async () => {
                try {
                    log.verbose(`Cron task begins: ${task.name} ${task.cronTime}`);
                    await task.process();
                    log.verbose(
                        `Cron task ends: ${task.name} ${task.cronTime}` +
                            ` -> Next send time: ${this.activeCrons[task.name].nextDate().toString()}`
                    );
                } catch (err) {
                    ErrorHandler.handle(
                        new CustomError(`Cron task ${task.name} failed.`, true, this.constructor.name, undefined, err),
                        log
                    );
                }
            },
            null,
            false,
            "Europe/Prague"
        );
    }

    /**
     * Starts all registered cron tasks
     */
    public startAll(): void {
        for (const cronName in this.activeCrons) {
            if (this.activeCrons.hasOwnProperty(cronName)) {
                this.activeCrons[cronName].start();
            }
        }
    }

    /**
     * Stops all registered cron tasks
     */
    public stopAll(): void {
        for (const cronName in this.activeCrons) {
            if (this.activeCrons.hasOwnProperty(cronName)) {
                this.activeCrons[cronName].stop();
            }
        }
    }

    /**
     * Checks status of all registered cron tasks
     */
    public checkStatus(): void {
        for (const cronName in this.activeCrons) {
            if (this.activeCrons.hasOwnProperty(cronName)) {
                log.info(
                    `Cron '${cronName}' is running: ${this.activeCrons[cronName].running}` +
                        ` -> Next send time: ${this.activeCrons[cronName].nextDate().toString()}`
                );
            }
        }
    }

    /**
     * Gets all running cron tasks
     */
    public getRunningTasks(): { [name: string]: CronJob } {
        const running = { ...this.activeCrons };
        for (const cronName in running) {
            if (running.hasOwnProperty(cronName)) {
                if (!running[cronName].running || running[cronName].running === false) {
                    delete running[cronName];
                }
            }
        }
        return running;
    }

    /**
     * Gets all stopped cron tasks
     */
    public getStoppedTasks(): { [name: string]: CronJob } {
        const stopped = { ...this.activeCrons };
        for (const cronName in stopped) {
            if (stopped.hasOwnProperty(cronName)) {
                if (stopped[cronName].running || stopped[cronName].running === true) {
                    delete stopped[cronName];
                }
            }
        }
        return stopped;
    }
}

const cronTasksManger = new CronTasksManger();

export { cronTasksManger };
