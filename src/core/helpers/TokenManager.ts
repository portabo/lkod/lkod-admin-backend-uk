import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import * as fs from "fs";
import { Algorithm, sign, verify } from "jsonwebtoken";
import { v4 } from "uuid";
import { config } from "../config";
import { redisConnector } from "../database";
import { log } from "../helpers";

/**
 * Token Manager class to manage JWT tokens (creation, verification).
 */
class TokenManager {
    /** JWT encoding algorithm */
    private algorithm: Algorithm;
    /** JWT public key for token verification. Loaded from file */
    private publicKey: Buffer;
    /** JWT private key for token generation. Loaded from file */
    private privateKey: Buffer;

    constructor() {
        this.algorithm = "RS512";
        try {
            this.publicKey = fs.readFileSync("./keys/key.pub");
            this.privateKey = fs.readFileSync("./keys/key.key");
        } catch (err) {
            throw new CustomError("Path to cert must be set.", false, this.constructor.name, 500, err);
        }
    }

    /**
     * JWT token generating
     *
     * @param {any} data
     * @param {number|string} ttl (optional) token validity
     * @returns {Promise<string>} JWT token
     */
    public async generateToken(data: any, ttl: number | string | null = null): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            sign(
                data,
                this.privateKey,
                {
                    algorithm: this.algorithm,
                    ...(ttl ? { expiresIn: ttl } : {}),
                    issuer: config.backend_url,
                    jwtid: v4(),
                },
                (err, token) => {
                    if (err) {
                        return reject(new CustomError("Error while JWT token generation", true, this.constructor.name, 500, err));
                    }
                    return resolve(token ?? "");
                }
            );
        });
    }

    /**
     * JWT token verification and decoding
     *
     * @param {string} token JTW token
     * @returns {Promise<string|object>} decoded JWT
     */
    public async verifyAndDecodeToken(token: string): Promise<string | object> {
        return new Promise<any>((resolve, reject) => {
            verify(token, this.publicKey, { algorithms: [this.algorithm] }, (err, decoded) => {
                if (err) {
                    return reject(new CustomError("Error while JWT token validation", true, this.constructor.name, 401, err));
                }
                return resolve(decoded);
            });
        });
    }

    /**
     * Adding token to redis blacklist.
     *
     * @param {string} jti JWT token id
     * @param {number} exp token expiration in seconds
     * @returns {Promise<boolean>}
     */
    public async setTokenToBlacklist(jti: string, exp?: number): Promise<boolean> {
        try {
            const redisConnection = redisConnector.getConnection();
            if (!exp) {
                await redisConnection.set(
                    jti, // JWT token id
                    1 // some data
                );
            } else {
                await redisConnection.setex(
                    jti, // JWT token id
                    exp - Math.floor(Date.now() / 1000), // JWT exp - now in seconds
                    1 // some data
                );
            }
            return true;
        } catch (err) {
            throw new CustomError(`Saving token to blacklist failed. ${jti} ${exp}`, true, this.constructor.name, 500, err);
        }
    }

    /**
     * Checking if token is blacklisted.
     *
     * @param {string} token JWT token
     * @returns {Promise<boolean>}
     */
    public async isTokenBlacklisted(token: string): Promise<boolean> {
        try {
            const redisConnection = redisConnector.getConnection();
            const decoded = (await tokenManager.verifyAndDecodeToken(token)) as { jti: string; exp: number };
            return (await redisConnection.exists(decoded.jti)) === 1;
        } catch (err) {
            log.debug("Checking if token is blacklisted failed.");
            log.debug(err);
            return false;
        }
    }
}

const tokenManager = new TokenManager();

export { tokenManager };
