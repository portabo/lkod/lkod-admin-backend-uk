export interface IDatasetQueryOptions {
    limit?: number;
    offset?: number;
    filter?: {
        organizationId?: number;
        status?: string[];
        themeIris?: string[];
        publisherIri?: string;
        keywords?: string[];
        formatIris?: string[];
        searchString?: string;
    };
}
