import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import axios, { AxiosPromise } from "axios";
import { config } from "../config";
import { IVocabulary } from "../config/ConfigInterface";
import { log } from "../helpers";

/**
 * Sparql Connection Strategy class to handle communication with SPARQL endpoint
 */
export class SparqlConnectionStrategy {
    /**
     * Uploads dataset into Graph Store
     *
     * @param {any} dataset
     * @returns {boolean}
     */
    public static async uploadDataset(dataset: any): Promise<boolean> {
        try {
            const response = await axios({
                data: JSON.stringify(dataset),
                headers: {
                    "Cache-Control": "no-cache",
                    "Content-Type": "application/ld+json",
                    ...(config.sparql.auth && { Authorization: config.sparql.auth }),
                },
                method: "POST",
                url: config.sparql.data_url,
            });

            if (![200, 204].includes(response.status)) {
                throw new Error(`[${response.status}] ${JSON.stringify(response.data)}`);
            }
            return true;
        } catch (err) {
            throw new CustomError("Error while uploading dataset", true, this.constructor.name, 500, err);
        }
    }

    /**
     * Removes dataset from Graph Store
     *
     * @param {string} datasetIri
     * @returns {boolean}
     */
    public static async removeDatasetCascade(datasetIri: string): Promise<boolean> {
        try {
            const response = await axios({
                data:
                    `DELETE { ?s ?p ?o. ?o ?p1 ?o1. } ` +
                    `WHERE { ` +
                    `    FILTER(regex(str(?s), "${datasetIri}" ) )` +
                    `    ?s ?p ?o. ` +
                    `    OPTIONAL { ?o ?p1 ?o1. FILTER (isBlank(?o)) } ` +
                    `} `,
                headers: {
                    Accept: "*/*",
                    "Content-Type": "application/sparql-update",
                    ...(config.sparql.auth && { Authorization: config.sparql.auth }),
                },
                method: "POST",
                responseType: "text",
                url: config.sparql.update_url,
            });

            if (![200, 204].includes(response.status)) {
                throw new CustomError(JSON.stringify(response.data), true, undefined, response.status);
            }

            return true;
        } catch (err) {
            throw new CustomError("Error while removing dataset", true, this.constructor.name, 500, err);
        }
    }

    /**
     * Uploads vocabularies
     *
     * @param vocabularies
     */
    public static async uploadVocabularies(vocabularies: IVocabulary[]): Promise<void> {
        try {
            const fileResponses = await Promise.allSettled<AxiosPromise>(
                vocabularies.map((vocab) => {
                    return axios({
                        method: "GET",
                        url: vocab.sourceUrl,
                    });
                })
            );

            const toUpload: Array<{ index: number; contentType: string; data: any }> = [];
            for (const [i, fileResponse] of fileResponses.entries()) {
                if (fileResponse.status === "fulfilled") {
                    toUpload.push({
                        index: i,
                        contentType: vocabularies[i].contentType,
                        data: fileResponse.value.data,
                    });
                } else {
                    log.error(`Vocabulary file was not downloaded. ID: ${vocabularies[i].id}`);
                    log.error(fileResponse.reason);
                }
            }

            const sparqlResponses = await Promise.allSettled<AxiosPromise>(
                toUpload.map(async (file) => {
                    // clear original data
                    await Promise.all(
                        vocabularies[file.index].iriPrefixes.map((iriPrefix) => {
                            return this.removeDatasetCascade(iriPrefix);
                        })
                    );

                    // upload new data
                    return axios({
                        data: file.data,
                        headers: {
                            "Cache-Control": "no-cache",
                            "Content-Type": file.contentType,
                            ...(config.sparql.auth && { Authorization: config.sparql.auth }),
                        },
                        method: "POST",
                        url: config.sparql.data_url,
                    });
                })
            );

            for (const [i, sparqlResponse] of sparqlResponses.entries()) {
                if (sparqlResponse.status === "rejected") {
                    log.error(`Vocabulary was not uploaded. ID: ${vocabularies[i].id}`);
                    log.error(sparqlResponse.reason);
                } else {
                    log.verbose(`Vocabulary was successfully uploaded. ID: ${vocabularies[i].id}`);
                    log.debug(sparqlResponse.value.data);
                }
            }
        } catch (err) {
            throw new CustomError("Error while uploading vocabularies", true, this.constructor.name, 500, err);
        }
    }
}
