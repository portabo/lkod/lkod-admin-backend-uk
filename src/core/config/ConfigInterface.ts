import { MailerTansportType } from "../mailer/Mailer";
import { ISMTPMailConfig } from "../mailer/SMTPMail.service";
import { IFtpConfig } from "../storage/Ftp.service";
import { IS3Config } from "../storage/S3.service";
import { StorageType } from "../storage/Storage";

export interface IVocabulary {
    id: string;
    sourceUrl: string;
    contentType: string;
    iriPrefixes: string[];
}

export interface IConfig {
    activity_log_enabled: boolean;
    app_version: string;
    backend_url: string;
    database: {
        database?: string;
        host: string;
        port: number;
        password?: string;
        schema: string;
        username?: string;
        ssl?: boolean;
    };
    frontend_url: string;
    catalog_frontend_url: string;
    linked_data: {
        catalog_context: string;
        catalog_publisher_id: string;
        catalog_name: string;
        catalog_description: string;
        catalog_homepage: string;
        poskytovatel_url_prefix: string;
    };
    log_level?: string;
    mailerFrom: string;
    mailerType: MailerTansportType;
    node_env?: string;
    pathPrefix: string;
    port?: string;
    redis_connection?: string;
    session_expires_in: number;
    smtp: ISMTPMailConfig;
    sparql: {
        auth?: string;
        query_url: string;
        data_url: string;
        update_url: string;
        vocabulary: {
            crontime: string;
            static_data: IVocabulary[];
            organizations_crontime: string;
        };
    };
    arcgis: {
        enabled: boolean;
        cronRefreshTime: string;
    };
    storageType: StorageType;
    storageFileMaxSizeInKB: number;
    ftp: IFtpConfig;
    s3: IS3Config;
    token_expires_in: number;
    datasetLookupCacheInMinutes: number;
}
