import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";

/**
 * Base Router class
 */
export class BaseRouter {
    constructor() {
        //
    }

    /**
     * Handles errors from `express-validator`
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     */
    public handleValidationError = (req: Request, res: Response, next: NextFunction): void => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return next(new CustomError("Bad Request", true, this.constructor.name, 400, errors.array()));
        }
        return next();
    };
}
