import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { literal } from "sequelize";
import { IOrganizationAttributes, OrganizationModel } from "../../models/OrganizationModel";

/**
 * Organization Controller class
 */
export class OrganizationsController {
    constructor() {
        //
    }

    /**
     * Returns all user's organizations
     *
     * @param {number} userId
     * @returns {Promise<IOrganizationAttributes[]>}
     */
    public async getAllOrganizations(userId: number): Promise<IOrganizationAttributes[]> {
        try {
            return await OrganizationModel.findAll({
                attributes: {
                    exclude: ["arcGisFeed"],
                    include: [[literal('"arcGisFeed" IS NOT NULL'), "hasArcGisFeed"]],
                },
                include: [
                    {
                        association: OrganizationModel.associations.users,
                        attributes: [],
                        through: { attributes: [] },
                        where: {
                            id: userId,
                        },
                    },
                ],
            });
        } catch (err) {
            throw new CustomError("Error while getting all Organizations", true, this.constructor.name, 500, err);
        }
    }
}
