import { NextFunction, Response, Router } from "express";
import { AuthenticationMiddleware, IExtendedRequest } from "../../core/middlewares";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { OrganizationsController } from "./";

/**
 * Organiations Router class
 */
export class OrganizationsRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    public controller: OrganizationsController;

    constructor() {
        super();
        this.controller = new OrganizationsController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get("/", new AuthenticationMiddleware().authenticate, this.getAllOrganizations);
    };

    /**
     * Getting organizations
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getAllOrganizations = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const organizations = await this.controller.getAllOrganizations(req.decoded?.id || 0);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(organizations);
        } catch (err) {
            next(err);
        }
    };
}
