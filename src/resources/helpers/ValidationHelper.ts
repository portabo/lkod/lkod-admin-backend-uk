import { query } from "express-validator";
import { ParamValidatorManager } from "./ParamValidatorManager";

export default class ValidationHelper {
    public static getCommonFilters = () => {
        return [
            query("organizationId").optional().isNumeric(),
            query("status").optional().custom(ParamValidatorManager.getInstance().getStatusValidator()),
            query("status.*").isString().custom(ParamValidatorManager.getInstance().getStatusValidator()),
            query("searchString").optional().isString(),
            query("publisherIri").optional().isString().custom(ParamValidatorManager.getInstance().getPublisherValidator()),
            query("themeIris").optional().custom(ParamValidatorManager.getInstance().getThemeValidator()),
            query("themeIris.*").isString().custom(ParamValidatorManager.getInstance().getThemeValidator()),
            query("keywords").optional().custom(ParamValidatorManager.getInstance().getKeywordValidator()),
            query("keywords.*").isString().custom(ParamValidatorManager.getInstance().getKeywordValidator()),
            query("formatIris").optional().custom(ParamValidatorManager.getInstance().getFormatValidator()),
            query("formatIris.*").isString().custom(ParamValidatorManager.getInstance().getFormatValidator()),
            query("limit").optional().isNumeric(),
            query("offset").optional().isNumeric(),
        ];
    };
}
