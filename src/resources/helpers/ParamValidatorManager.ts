import { CustomValidator } from "express-validator";
import CachedLookups, { LookupFilter } from "../../core/helpers/LookupQuery/CachedLookups";
import { DatasetStatus } from "../../models/DatasetModel";

export class ParamValidatorManager {
    private static instance: ParamValidatorManager;
    private lookupHelper: CachedLookups;

    private constructor() {
        this.lookupHelper = CachedLookups.getInstance();
    }

    public static getInstance = (): ParamValidatorManager => {
        if (!this.instance) {
            this.instance = new ParamValidatorManager();
        }

        return this.instance;
    };

    public getStatusValidator = (): CustomValidator => {
        return async (value, { req }) => {
            const allowedValues = Object.values(DatasetStatus).filter((el) => el !== DatasetStatus.deleted) as string[];
            if (typeof value === "string" && !allowedValues.includes(value)) {
                return Promise.reject("Invalid status value.");
            }
            return true;
        };
    };

    public getThemeValidator = (): CustomValidator => {
        return async (value, { req }) => {
            if (typeof value === "string" && !(await this.lookupHelper.getAllowedValues(LookupFilter.themes)).includes(value)) {
                return Promise.reject("Invalid theme iri value.");
            }
            return true;
        };
    };

    public getPublisherValidator = (): CustomValidator => {
        return async (value, { req }) => {
            if (
                typeof value === "string" &&
                !(await this.lookupHelper.getAllowedValues(LookupFilter.publishers)).includes(value)
            ) {
                return Promise.reject("Invalid publisher value.");
            }
            return true;
        };
    };

    public getFormatValidator = (): CustomValidator => {
        return async (value, { req }) => {
            if (typeof value === "string" && !(await this.lookupHelper.getAllowedValues(LookupFilter.formats)).includes(value)) {
                return Promise.reject("Invalid format iri value.");
            }
            return true;
        };
    };

    public getKeywordValidator = (): CustomValidator => {
        return async (value, { req }) => {
            if (typeof value === "string" && !(await this.lookupHelper.getAllowedValues(LookupFilter.keywords)).includes(value)) {
                return Promise.reject("Invalid keyword value.");
            }
            return true;
        };
    };
}
