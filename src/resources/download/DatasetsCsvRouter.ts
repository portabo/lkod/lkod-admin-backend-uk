import { NextFunction, Response, Router } from "express";
import { AuthenticationMiddleware, IExtendedRequest } from "../../core/middlewares";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { DatasetsCsvController } from "./DatasetsCsvController";

export class DatasetsCsvRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private controller: DatasetsCsvController;

    constructor() {
        super();
        this.controller = new DatasetsCsvController();
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.get("/", [], this.handleValidationError, new AuthenticationMiddleware().authenticate, this.downloadDatasets);
    };

    private downloadDatasets = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.handleResponse(req.decoded?.id || 0, res);
        } catch (err) {
            next(err);
        }
    };
}
