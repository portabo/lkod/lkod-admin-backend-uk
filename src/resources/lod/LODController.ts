import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { config } from "../../core/config";
import { DatasetModel, DatasetStatus, IDatasetData } from "../../models/DatasetModel";
import { OrganizationModel } from "../../models/OrganizationModel";

/**
 * LOD (Linked Open Data) Controller class
 */
export class LODController {
    /**
     * Returns LOD Catalog
     *
     * @returns {any} Catalog definition
     */
    public async getCatalog({ originalUrl, publishers }: { originalUrl: string; publishers: string[] }): Promise<any> {
        try {
            const isOneValueQuery = publishers && Array.isArray(publishers) && publishers.length === 1;

            const isMultipleValuesQuery = publishers && Array.isArray(publishers) && publishers.length > 1;

            const whereCondition = publishers
                ? {
                      slug: [...publishers],
                  }
                : {};

            const datasets = await DatasetModel.findAll({
                include: {
                    model: OrganizationModel,
                    as: "organization",
                    attributes: ["slug", "name", "description"],
                    where: whereCondition,
                },
                attributes: ["data", "organizationId"],
                where: {
                    status: DatasetStatus.published,
                },
            });

            const uniqueOrganizationNamesArray = datasets
                .map((dataset) => dataset.organization.dataValues.name)
                .filter((name, index, array) => array.indexOf(name) === index);

            const createCatalogName = isOneValueQuery
                ? `${uniqueOrganizationNamesArray.toString()} - Katalog otevřených dat Golemio`
                : isMultipleValuesQuery
                ? "Sdružený katalog otevřených dat Golemio"
                : config.linked_data.catalog_name;

            const createCatalogDescription = isOneValueQuery
                ? `${datasets[0]?.organization?.dataValues?.description || ""}`
                : isMultipleValuesQuery
                ? `Katalog obsahuje datové sady organizací ${uniqueOrganizationNamesArray.join(", ").toString()}`
                : config.linked_data.catalog_description;

            const catalog = {
                "@context": config.linked_data.catalog_context,
                iri: `${config.backend_url}${config.pathPrefix}${originalUrl}`,
                poskytovatel: `${config.linked_data.poskytovatel_url_prefix}${config.linked_data.catalog_publisher_id}`,
                typ: "Katalog",
                název: {
                    cs: createCatalogName,
                },
                popis: {
                    cs: createCatalogDescription,
                },
                domovská_stránka: config.linked_data.catalog_homepage,
                datová_sada: [] as string[],
            };

            catalog.datová_sada = datasets.map((dataset) => dataset.data?.iri).filter((d) => d !== undefined) as string[];

            return catalog;
        } catch (err) {
            throw new CustomError("Error while getting LOD Catalog", true, this.constructor.name, 500, err);
        }
    }

    /**
     * Returns dataset data (form data in json-ld)
     *
     * @param {string} datasetId UUID
     * @returns {IDatasetData | {}}
     */
    public async getDatasetById(datasetId: string): Promise<IDatasetData | {}> {
        try {
            const dataset = await DatasetModel.findOne({
                attributes: ["data"],
                where: {
                    id: datasetId,
                    status: DatasetStatus.published,
                },
            });

            if (!dataset) {
                throw new CustomError(`Dataset was not found`, true, this.constructor.name, 404);
            }

            return dataset.data || {};
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while Dataset by ID", true, this.constructor.name, 500, err);
            }
        }
    }
}
