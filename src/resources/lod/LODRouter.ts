import { NextFunction, Response, Router } from "express";
import { param } from "express-validator";
import { IExtendedRequest } from "../../core/middlewares";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { LODController } from "./";

/**
 * LOD (Linked Open Data) Router class
 */
export class LODRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    public controller: LODController;

    constructor() {
        super();
        this.controller = new LODController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get("/", this.getCatalog);
        this.router.get(
            "/:datasetId",
            [param("datasetId").notEmpty().isString(), param("publishers").optional().toArray()],
            this.handleValidationError,
            this.getDatasetById
        );
    };

    /**
     * Getting catalog
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getCatalog = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const catalog = await this.controller.getCatalog({
                originalUrl: req.originalUrl,
                publishers: req.query?.publishers as string[],
            });
            res.setHeader("Content-Type", "application/ld+json; charset=utf-8");
            res.status(200).json(catalog);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Getting dataset data
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getDatasetById = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const dataset = await this.controller.getDatasetById(req.params.datasetId);
            res.setHeader("Content-Type", "application/ld+json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };
}
