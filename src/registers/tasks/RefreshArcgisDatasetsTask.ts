import { ILogger } from "@golemio/core/dist/helpers";
import { ICronTask } from "../../core/helpers/CronTasksManager";
import { SparqlConnectionStrategy } from "../../core/strategies/SparqlConnectionStrategy";
import { DatasetModel, DatasetStatus } from "../../models/DatasetModel";
import { OrganizationRepository } from "../../models/OrganizationRepository";
import { config } from "../../core/config";

/**
 * Cron task to refresh arcgis related datasets in sparql
 */
export default class RefreshArcgisDatasetsTask implements ICronTask {
    public readonly cronTime = config.arcgis.cronRefreshTime;
    public readonly name = "refresh_arcgis_datasets";

    constructor(private organizationRepository: OrganizationRepository, private log: ILogger) {}

    public async process(): Promise<void> {
        const organizations = await this.organizationRepository.getArcGisOrgIds();

        for (const orgId of organizations) {
            const datasets = await DatasetModel.findAll({
                where: {
                    organizationId: orgId,
                    status: "saved",
                },
            });

            for (const dataset of datasets) {
                if (!!dataset.updatedAt) {
                    await SparqlConnectionStrategy.removeDatasetCascade(dataset.data!.iri!);
                }
                await SparqlConnectionStrategy.uploadDataset(dataset.data!);
                await DatasetModel.update(
                    {
                        status: DatasetStatus.published,
                    },
                    {
                        where: {
                            id: dataset.id,
                        },
                    }
                );
            }

            const deletedDatasets = await DatasetModel.findAll({
                where: {
                    organizationId: orgId,
                    status: "deleted",
                },
            });

            for (const dataset of deletedDatasets) {
                await SparqlConnectionStrategy.removeDatasetCascade(dataset.data!.iri!);
                await DatasetModel.destroy({
                    where: {
                        id: dataset.id,
                    },
                });
            }
        }
        this.log.verbose(`Arcgis related datasets refreshed.`);
    }
}
