# Golemio LKOD Backend

Refer to the API documentation at:

-   [Main (production) LKOD API Documentation](https://api.opendata.praha.eu/api-docs/)
-   [Test (development) LKOD API Documentation](https://api-lkod.rabin.golemio.cz/api-docs/)

Developed by http://operatorict.cz

## Local Installation

### Prerequisites

- NodeJS v18 (https://nodejs.org)
- PostgreSQL v13 (https://www.postgresql.org/)
  - extensions: unaccent, pgcrypto
- npm (https://www.npmjs.com/)
- TypeScript (https://www.typescriptlang.org/)

### Installation

Install all prerequisites

Install all dependencies using command:
```
npm install
```

from the application's root directory.

### Build & Run

Running the application in any way will load all config variables from environment variables or the .env file. To run, set all environment variables from the `.env.template` file, or copy the `.env.template` file into new `.env` file in root directory and set variables there.

Project uses `dotenv` package: https://www.npmjs.com/package/dotenv

#### Production

To compile typescript code into js one-time (production build):
```
npm run build
```
To run the app:

```
npm start
```

#### Dev/debug

Run via TypeScript (in this case it is not needed to build separately, application will watch for changes and restart on save):
```
npm run dev-start
```
or run with a debugger:
```
npm run dev-start-debug
```

Application is now running locally on port 3000 or on port specified in the environment variable.


## DB migrations

For setting up database and running the migrations, run:
```
npm run migrate-db
```

The example (dummy) data is in the file `db/dummy-data.sql`.

## New User or Organization creation

To create a new user/organization in DB you can use CLI script which generates SQL insert script with new user/organization info. To get help with CLI scripts, run:
```sh
bin/lkod.js --help
bin/lkod.js create-user --help
bin/lkod.js create-organization --help
bin/lkod.js add-user-to-organization --help
```
From docker, run:
```sh
docker build -t lkod-backend
docker run [-v mandatory config files/envs] --name lkod-backed lkod-backed
docker exec lkod-backend sh -c 'bin/lkod.js --help'
docker exec lkod-backend sh -c 'bin/lkod.js create-user --help'
docker exec lkod-backend sh -c 'bin/lkod.js create-organization --help'
docker exec lkod-backend sh -c 'bin/lkod.js add-user-to-organization --help'
```

The second way is to use direct sql script with PostgreSQL pgcrypto extension.
```sql
CREATE EXTENSION pgcrypto;

/* create-user */
INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES
('test@golemio.cz',crypt('pass', gen_salt('bf', 10)),'test user','user','true');

/* create-organization */
INSERT INTO "organization" ("name", "identificationNumber", "slug", "arcGisFeed") VALUES
('test org','2795281','test-org','https://xxxxx.arcgis.com/api/feed/dcat-us/1.1.json');

/* add-user-to-organization */
INSERT INTO "user_organization" ("userId", "organizationId") VALUES
('1','1');
```

## Tests

To run all test defined in /test directory simply run this command:
```
npm test
```
from the application's root directory. All tests should pass.

## Configuration

### ENV

Environment variables are described at `.env.template` file.

### JWT Keys

To create JWT keys run commands above:
```bash
ssh-keygen -t rsa -b 4096 -m PEM -N '' -f keys/key.key
openssl rsa -in keys/key.key -pubout -outform PEM -out keys/key.pub
chmod 644 keys/key.key
chmod 644 keys/key.pub
```

It's necessary for JWT token sign.

### (optional) Static RDF vocabularies

The RDF vocabularies are defined in `config/rdf-static-data.json`. This config contain paths to metadata sources which are used mainly for frontend apps (lkod-catalog, lkod-frontend). This config not affect dataset publishing to NKOD.

## Logging

Logging uses `@golemio/core` logger which uses `pino`.

You can set both `LOG_LEVEL` and `DEBUG` settings in ENV variables.

## Documentation

For generating documentation run `npm run generate-docs`. Typedoc source code documentation is located in `docs/typedoc`.

More documentation in `docs/`.

### API specification

The REST API specification is located in `docs/openapi.yaml`.

## Troubleshooting

Contact vyvoj@operatorict.cz
