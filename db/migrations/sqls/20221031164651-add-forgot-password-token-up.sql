CREATE TABLE "forgot_password_token"
(
    "id" uuid NOT NULL,
    "userId" bigint NOT NULL,
    "createdAt" timestamp with time zone NOT NULL DEFAULT NOW(),
    "validTo" timestamp with time zone NOT NULL,
    CONSTRAINT forgot_password_token_pkey PRIMARY KEY (id)
);
