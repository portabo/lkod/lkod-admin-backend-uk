ALTER TABLE dataset ALTER COLUMN "data" TYPE jsonb USING "data"::jsonb;

CREATE INDEX dataset_data_gin ON dataset USING GIN("data");
