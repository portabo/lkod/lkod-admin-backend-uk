
/* password = 'pass' */
INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES
    ('test@golemio.cz','$2b$10$PCAwdjhktHpwOBJBW7RIFeGD8IYXiSMIohRdG83hl8j.lerlWOwP.','test user','user','false');

INSERT INTO "organization" ("name", "identificationNumber", "slug", "logo", "description", "arcGisFeed") VALUES
    ('test org',              '02795281', 'test-org',        'https://placehold.co/100x100', 'Lorem ipsum dolor sit amet.', NULL),
    ('arcgis feed test org',  '02795281', 'test-arcgis-org', 'https://placehold.co/100x100', 'Lorem ipsum dolor sit amet.', 'https://test-data-brno-cz-mestobrno.hub.arcgis.com/api/feed/dcat-us/1.1.json');

INSERT INTO "user_organization" ("userId", "organizationId") VALUES
    ('1','1');
