# Organizations

Organization has two main usage:
- user authorization and dataset access permission control
- metadata management for SPARQL dataset `publishers`

Database tables `organization` and `user_organization` control access permissions. This table is used for dataset administration, dataset publishing to SPARQL (apps lkod-fronted and lkod-backend) and generate RDF organization metadata to SPARQL.

RDF metadata are periodically uploaded to SPARQL. Periodicity depends on env variable `SPARQL_ORGANIZATIONS_CRONTIME`.

Binding between database and RDF is the organization slug:
- database column `slug` which is filled into dataset publisher iri
- RDF property iri (rdf:about) which contains the slug
- env variable `POSKYTOVATEL_URL_PREFIX` which defines publisher iri prefix
